package dao

import model.ItemMenuModel
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class ItemMenuDao(val db : Database) : DAOInterfaceItem{
    override fun init() = transaction(db) {
        SchemaUtils.create(Restaurant)
    }

    override fun createItem(item: ItemMenuModel) = transaction(db) {
        ItemMenu.insert{
            it[ItemMenu.idRestau] = item.idRestau
            it[ItemMenu.nom] = item.nom
            it[ItemMenu.description] = item.description
            it[ItemMenu.prix] = item.prix
            it[ItemMenu.categorie] = item.categorie
        }
        Unit
    }

    override fun updateItem(id: Int, idRestau: Int, nom: String, description: String, prix: Double, categorie: String) = transaction(db) {
        ItemMenu.update({ItemMenu.id eq id}){
            it[ItemMenu.idRestau] = idRestau
            it[ItemMenu.nom] = nom
            it[ItemMenu.description] = description
            it[ItemMenu.prix] = prix
            it[ItemMenu.categorie] = categorie
        }
        Unit
    }

    override fun deleteItem(id: Int) = transaction(db) {
        ItemMenu.deleteWhere{ItemMenu.id eq id}
        Unit
    }

    override fun getItem(id: Int): model.ItemMenuModel? = transaction(db) {
        ItemMenu.select{ItemMenu.id eq id}.map{
            model.ItemMenuModel(
                it[ItemMenu.id],
                it[ItemMenu.idRestau],
                it[ItemMenu.nom],
                it[ItemMenu.description],
                it[ItemMenu.prix],
                it[ItemMenu.categorie]
            )
        }.singleOrNull()
    }

    override fun getAllItembyRestau(idRestau: Int): List<model.ItemMenuModel> = transaction(db) {
        ItemMenu.select ({ItemMenu.idRestau eq idRestau}).map{
            model.ItemMenuModel(
                it[ItemMenu.id],
                it[ItemMenu.idRestau],
                it[ItemMenu.nom],
                it[ItemMenu.description],
                it[ItemMenu.prix],
                it[ItemMenu.categorie]
            )
        }
    }

    override fun close() {

    }
}