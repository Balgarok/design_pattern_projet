package dao

import io.ktor.utils.io.core.*
import model.ClientModel

interface DAOInterface : Closeable {
    fun init()
    fun createClient(username: String, email: String, password: String, adresse: String)
    fun updateClient(id: Int, username: String, email: String, password: String, adresse: String)
    fun deleteClient(id: Int)
    fun getClient(id: Int): ClientModel?
    fun getAllClient(): List<ClientModel>
}