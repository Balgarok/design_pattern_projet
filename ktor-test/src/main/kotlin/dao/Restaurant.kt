package dao

import org.jetbrains.exposed.sql.Table

object Restaurant : Table(){
    val id = integer("id").primaryKey().autoIncrement()
    val nom = varchar("nom", 100)
    val adresse = varchar("adresse", 100)
    val categorie = varchar("categorie", 100)
}