package dao

import io.ktor.utils.io.core.*
import model.RestaurantModel

interface DAOInterfaceRestaurant : Closeable {
    fun init()
    fun createRestaurant(restau: RestaurantModel)
    fun updateRestaurant(id: Int, nom: String, adresse: String, categorie: String)
    fun deleteRestaurant(id: Int)
    fun getRestaurant(id: Int): RestaurantModel?
    fun getAllRestaurant(): List<model.RestaurantModel>
}