package dao

import org.jetbrains.exposed.sql.Table

object ItemMenu : Table(){
    val id = integer("id").primaryKey().autoIncrement()
    val idRestau = integer("id").autoIncrement()
    val nom = varchar("nom", 100)
    val description = varchar("description", 500)
    val prix = double("prix")
    val categorie = varchar("categorie", 100)
}