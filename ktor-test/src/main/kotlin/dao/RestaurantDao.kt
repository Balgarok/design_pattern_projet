package dao

import model.RestaurantModel
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction


class RestaurantDao(val db : Database) : DAOInterfaceRestaurant{
    override fun init() = transaction(db) {
        SchemaUtils.create(Restaurant)
    }

    override fun createRestaurant(restau: RestaurantModel) = transaction(db) {
        Restaurant.insert{
            it[Restaurant.nom] = restau.nom
            it[Restaurant.adresse] = restau.adresse
            it[Restaurant.categorie] = restau.categorie
        }
        Unit
    }

    override fun updateRestaurant(id: Int, nom: String, adresse: String, categorie: String) = transaction(db) {
        Restaurant.update({Restaurant.id eq id}){
            it[Restaurant.nom] = nom
            it[Restaurant.adresse] = adresse
            it[Restaurant.categorie] = categorie
        }
        Unit
    }

    override fun deleteRestaurant(id: Int) = transaction(db) {
        Restaurant.deleteWhere{Restaurant.id eq id}
        Unit
    }

    override fun getRestaurant(id: Int): RestaurantModel? = transaction(db) {
        Restaurant.select { Restaurant.id eq id}.map {
            model.RestaurantModel(
                it[Restaurant.id],
                it[Restaurant.nom],
                it[Restaurant.adresse],
                it[Restaurant.categorie]
            )
        }.singleOrNull()
    }

    override fun getAllRestaurant(): List<model.RestaurantModel> = transaction(db) {
        Restaurant.selectAll().map{
            model.RestaurantModel(
                it[Restaurant.id],
                it[Restaurant.nom],
                it[Restaurant.adresse],
                it[Restaurant.categorie]
            )
        }
    }

    override fun close() {

    }
}