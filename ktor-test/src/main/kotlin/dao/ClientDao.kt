package dao

import model.ClientModel
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class ClientDao(val db : Database) : DAOInterface{
    override fun init() = transaction(db) {
        SchemaUtils.create(Client)
    }
    override fun createClient(username: String, email: String, password: String, adresse: String) = transaction(db){
        Client.insert{
            it[Client.username] = username
            it[Client.email] = email
            it[Client.password] = password
            it[Client.adresse] = adresse
        }
        Unit
    }

    override fun updateClient(id: Int, username: String, email: String, password: String, adresse: String)  = transaction(db){
        Client.update({Client.id eq id}){
            it[Client.username] = username
            it[Client.email] = email
            it[Client.password] = password
            it[Client.adresse] = adresse
        }
        Unit
    }

    override fun deleteClient(id: Int)  = transaction(db){
        Client.deleteWhere{Client.id eq id}
        Unit
    }

    override fun getClient(id: Int): ClientModel?  = transaction(db){
        Client.select{Client.id eq id}.map {
            model.ClientModel(
                it[Client.id],
                it[Client.username],
                it[Client.email],
                it[Client.password],
                it[Client.adresse]
            )
        }.singleOrNull()
    }

    override fun getAllClient(): List<ClientModel>  = transaction(db){
        Client.selectAll().map {
            model.ClientModel(
                it[Client.id],
                it[Client.username],
                it[Client.email],
                it[Client.password],
                it[Client.adresse]
            )
        }
    }

    override fun close() {

    }
}