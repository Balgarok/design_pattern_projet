package dao

import io.ktor.utils.io.core.*
import model.ItemMenuModel

interface DAOInterfaceItem : Closeable {
    fun init()
    fun createItem(item: ItemMenuModel)
    fun updateItem(id: Int, idRestau: Int, nom : String, description: String, prix: Double, categorie: String)
    fun deleteItem(id: Int)
    fun getItem(id: Int): model.ItemMenuModel?
    fun getAllItembyRestau(idRestau: Int): List<ItemMenuModel>
}