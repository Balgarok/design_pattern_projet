package dao

import org.jetbrains.exposed.sql.Table


object Client : Table(){
    @JvmStatic
    val id = integer("id").primaryKey().autoIncrement()
    @JvmStatic
    val username = varchar("username", 100)
    @JvmStatic
    val email = varchar("email", 500)
    val password = varchar("password", 100)
    val adresse = varchar("adresse", 100)
}

