package model

data class ItemMenuModel(val id : Int, val idRestau: Int, val nom : String, val description : String, val prix: Double, val categorie: String)
