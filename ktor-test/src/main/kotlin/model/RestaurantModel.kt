package model

data class RestaurantModel(val id: Int, val nom: String, val adresse: String, val categorie: String)
