package com.test

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.test.plugins.*
import dao.ClientDao
import dao.ItemMenuDao
import dao.Restaurant
import dao.RestaurantDao
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import model.ItemMenuModel
import model.RestaurantModel
import org.jetbrains.exposed.sql.Database



fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


@Suppress("unused")
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false){
    install(io.ktor.features.ContentNegotiation){
        register(io.ktor.http.ContentType.Application.Json, JacksonConverter())
    }
    val daoC = ClientDao(Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver"))
    daoC.init()
    val daoR = RestaurantDao(Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver"))
    daoR.init()
    val daoI = ItemMenuDao(Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver"))
    daoI.init()

    routing {

        get("/restaurants"){
            call.respond(mapOf("restaurants" to daoR.getAllRestaurant()))
        }

        get("/itemmenus/{id}"){
            var id = call.parameters["id"]
            id?.let{
                call.respond(mapOf("itemmenus" to daoI.getAllItembyRestau(id.toInt())))
            }?:call.respond("Invalid Id")
        }

        post("/restaurant"){
            var restau = call.receive<RestaurantModel>()
            daoR.createRestaurant(restau)
            call.respondText("Restaurant staured correctly", status= HttpStatusCode.Created)
        }

        post("/itemmenu"){
            var item = call.receive<ItemMenuModel>()
            daoI.createItem(item)
            call.respondText("Item sored correctly", status= HttpStatusCode.Created)
        }
    }
}